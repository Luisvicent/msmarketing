<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDetalleEntrada extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('detalle_entrada', function (Blueprint $table) {
            $table->id();
            $table->BigInteger('idEntradas')->unsigned();
            $table->BigInteger('idProductos')->unsigned();
            $table->string('CantidadEntradas',50);
            $table->timestamps();
            $table->foreign('idEntradas')->references('id')->on('entradas');
            $table->foreign('idProductos')->references('id')->on('productos');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('detalle_entrada');
    }
}
