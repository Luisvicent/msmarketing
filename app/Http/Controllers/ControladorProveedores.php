<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Alert;
use DB;
use App\Proveedor;

class ControladorProveedores extends Controller
{
    public function index(){
    	return view('proveedores.index')->with(['proveedores'=>Proveedor::paginate(5)]);
    }

     public function create(){
        $proveedores=Proveedor::all();
    	return view('proveedores.crear',compact('proveedores'));
    }

    public function store(){
        alert()->success('Registro exitoso', 'Proveedor creado!')->persistent("Cerrar");
    	$proveedor = Proveedor::create(request()->all());
    	return redirect()
    		->route('proveedores.index');
    }

    public function edit($proveedor){
    	return view('proveedores.editar')->with(['proveedor'=>Proveedor::findOrFail($proveedor),]);

    }

    public function update($proveedor){
        alert()->success('Registro exitoso', 'Proveedor editado!')->persistent("Cerrar");
    	$proveedor = Proveedor::findOrFail($proveedor); //Buscar datos del proveedor a modificar
    	$proveedor -> update(request()->all()); //Actualizar el proveedor
    	return redirect()
    		->route('proveedores.index');
    }

    public function destroy($proveedorid){
    	alert()->success('Cambio de estado Exitoso', 'estado modificado!')->persistent("Cerrar");
        $nuevoestado = 0;
        //$proveedor = Proveedor::findOrFail($proveedor)->makeHidden([ 'id','NombreProveedor','Telefono','Correo','created_at','updated_at']); //Buscar datos del proveedor a modificar
        list($estado) = DB::table('proveedores')->select('estado')->where('id','=',$proveedorid)->get('estado');
        //$proveedorEstado = $proveedor->fetchAll();
        if ($estado->estado == 1) {
           $nuevoestado = 2;    
        }
        else{
            $nuevoestado = 1;
        }
        $proveedor =DB::table('proveedores')->where('id',$proveedorid)->update(['estado'=>$nuevoestado]); //Actualizar el proveedor
        return redirect()
            ->route('proveedores.index');
    }
}
