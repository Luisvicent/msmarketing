<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Clientes extends Model
{
    protected $fillable = [
        'id',
        'Tipo_identificacion',
        'nombre',
        'apellido',
        'edad',
        'celular',
    ];
}
