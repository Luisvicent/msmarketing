<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title>MsMarketing | Detalle entrada</title>

</head>
<body>
@extends('layouts.app')
@section('content')

	<div class="container">
	<h2>Detalle de Entrada<a class="btn btn-secondary float-right mb-3" style="margin:3px;" href="{{ url('Entradas') }}">Volver</a></h1>

	@if(empty($entradas))
		<div class="alert alert-warning">
			La lista de los detalles esta vacía
		</div>
	@else
		<div class="class-responsive">
			<table class="table table-striped">
				<thead class="">
					<tr>
						<th>Id Entrada</th>
						<th>Id Productos</th>
						<th>Cantidad Productos</th>



					</tr>
				</thead>
				<tbody>
					@foreach($entradas as $Detalle)
					<tr>
						<td>{{ $Detalle->identradas}}</td>
						<td>{{ $Detalle->productos}}</td>
						<td>{{ $Detalle->cantidad}}</td>

					</tr>
					@endforeach
				</tbody>
			</table>
		</div>
		{{-- Pagination --}}
        <div class="d-flex justify-content-center">
            {!! $entradas->links() !!}
        </div>
	@endif

@endsection
</body>
</html>
