<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<title>MsMarketing | Entradas</title>
	<link href="css/bootstrap.min.css" rel="stylesheet">
</head>
<body>
@extends('layouts.app')
@section('content')

	<div class="container">
	<h3>Listado de Entradas<a class="btn btn-success float-right mb-3" style="margin:3px;" href="{{ route('home') }}">Volver</a><a class="btn btn-primary float-right mb-3" style="margin:3px;" href="{{ route('Entradas.create') }}">Crear entrada</a>
	</h3>
	@if(empty($Entradas))
		<div class="alert alert-warning">
			La lista de docuementos esta vacia
		</div>
	@else
		<div class="class-resposive">
			<table class="table table-light table-hover  table-bordered">
				<thead class="">
					<tr>
						<th>Id Entradas</th>
						<th>Fecha de Entrada</th>
						<th>Total de Compra</th>
						<th>Acciones</th>


					</tr>
				</thead>
				<tbody>
					@foreach($Entradas as $entrada)
					<tr>
						<td>{{ $entrada->id }}</td>
						<td>{{ $entrada->FechaEntradas}}</td>
						<td>{{ $entrada->TotalEntradas}}</td>

						<td><a href="{{ route('Entradas.detail', ['entrada' => $entrada->id]) }}"><button type="button" class="btn btn-secondary  btn-sm"><i class="fas fa-eye"></i></button></a>
							<a href="{{ route('Entradas.edit', ['entrada' => $entrada->id]) }}"><button type="button" class="btn btn-primary btn-sm"><i class="fas fa-edit"></i></button></a>

							<form action="{{ route('Entradas.destroy', ['entrada' => $entrada->id]) }}" class="d-inline" method="post">
								@csrf
								@method('DELETE')
								<a href="{{ route('productos.verpdf') }}"><button type="button" class="btn btn-light btn-sm"><i class="fas fa-file-pdf"  style="color: rgb(230, 43, 43)"></i></button></a>
							</form>
						</td>
					</tr>
					@endforeach
				</tbody>
			</table>
		</div>
		{{-- Pagination --}}
        <div class="d-flex justify-content-center">
            {!! $Entradas->links() !!}
        </div>
	@endif
</body>
</html>
@endsection
