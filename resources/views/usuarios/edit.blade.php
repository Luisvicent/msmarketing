<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Msmarketing | Modificar usuario</title>

</head>

<body>
@extends('layouts.app')

@section('content')

<div class="container">
        <div class="row">
            <div class="col-sm-6">
            <h2>Editar usuario {{$user->name}}</h2>
                @if ($errors->any())


                @endif
            </div>
        </div>





    <form action="{{ route ('usuarios.update', $user->id) }}" method="POST" enctype="multipart/form-data">
        @method('PATCH')
        {{ csrf_field()}}

        <div class="row">
            <div class="form-group col-md-6">
                <label>Nombre</label>
                <input type="text" name="name" class="form-control {{$errors->has('name')?'is-invalid':('')}}"  placeholder="Nombre"
                    value="{{isset($user->name)?$user->name:old('name')}}">
                    {!! $errors->first('name', '<div class="invalid-feedback">El campo nombre es obligatorio y debe tener min 6 caracteres</div>')!!}


            </div>
            <div class="form-group col-md-6">
                <label>Email</label>
                <input type="email" class="form-control {{$errors->has('email')?'is-invalid':('')}} " value="{{$user->email}}" name="email" placeholder="Email">
                {!! $errors->first('email', '<div class="invalid-feedback">El campo email es obligatorio</div>')!!}




            </div>
        </div>

        <div class="row">
            <div class="form-group col-md-6">
                <label>Contraseña <span class="small">(Opcional)</span></label>
                <input type="password" name="password" class="form-control" placeholder="Contraseña">
            </div>
            <div class="form-group col-md-6">
                <label>Confirmar contraseña<span class="small">(Opcional)</span></label>
                <input type="password" class="form-control" name="password_confirmation" placeholder="Confirmar contraseña">
            </div>
        </div>

        <div class="row">
            <div class="form-group col-md-6">
                <label>Rol</label>
                <select name="rol" class="form-control">
                    <option selected disabled>Elige un rol para este usuario</option>
                    @foreach($roles as $role )
                    @if($role->name == str_replace(array( '["','"]'),'', $user->tieneRol()))
                    <option value="{{$role->id}}" selected>{{$role->name}}</option>


                    @else
                    <option value="{{$role->id}}">{{$role->name}}</option>

                    @endif

                    @endforeach
                </select>

            </div>
            <div class="form-group col-md-6">
                <label> Imagen</label>
                <input type="file" name="imagen" class="form-control">
                @if($user->imagen != "")
                <img src="{{asset  ('imagenes/'.$user->imagen) }}" alt="{{ $user->imagen }}" height="50px" width="50px">
                @endif
            </div>
        </div>

        <div class="row">
            <div class="form-group col-md-6">
                <button type="submit" class="btn btn-primary">Guardar</button>
                <button type="reset" class="btn btn-danger">Borrar Campos</button>
                 <a href="{{url('usuarios')}}" class="btn btn-primary">regresar</a>
            </div>
        </div>
    </form>
</div>
</div>
</div>



@endsection

</body>
</html>
