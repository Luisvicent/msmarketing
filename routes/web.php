<?php

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;






Auth::routes();

Route::get('/', 'HomeController@index')->name('home');
/* Route::get('/usuarios', 'UserController@index'); */

Route::resource('usuarios', 'UserController');/* ->middleware('auth'); */
/* Route::get('/clientes', function () {
    return view('clientes.index');
}); */

Route::resource('roles', 'RoleController');



Route::get('clientes', 'ClientestController@Index')->name('clientes.index');
Route::resource('clientes', 'ClientesController');
Route::match(['put', 'patch'], 'clientes/{id}','ClientesController@update')->name('clientes.update');
Route::delete('clientes/{id}','ClientesController@destroy')->name('clientes.destroy');


Route::get('Categorias', 'CategoriasController@index')->name('Categorias.index');
Route::resource('Categorias', 'CategoriasController');
Route::match(['put', 'patch'], 'Categorias/{id}','CategoriasController@update')->name('Categorias.update');
Route::delete('Categorias/{id}','CategoriasController@destroy')->name('Categorias.destroy');


//routes para la muestra de seleccion de Notas
Route::resource('/notas/todas', 'NotasController');
Route::get('/notas/favoritas', 'NotasController@favoritas');
Route::get('/notas/archivadas', 'NotasController@archivadas');

//productos
Route::get('productos', 'ControladorProductos@index')->name('productos.index');

Route::get('productos/crear', 'ControladorProductos@create')->name('productos.create');

Route::post('productos', 'ControladorProductos@store')->name('productos.store');

Route::get('productos/{producto}/editar', 'ControladorProductos@edit')->name('productos.edit');

Route::match(['put', 'patch'], 'productos/{producto}/editar', 'ControladorProductos@update')->name('productos.update');

Route::delete('producto/{producto}', 'ControladorProductos@destroy')->name('productos.destroy');
Auth::routes();

Route::get('productos/verpdf', 'ControladorProductos@verpdf')->name('productos.verpdf');




//Entradas
Route::get('Entradas', 'ControladorEntradas@index')->name('entradas.index');

Route::get('Entradas/crear', 'ControladorEntradas@create')->name('Entradas.create');

Route::post('Entradas', 'ControladorEntradas@store')->name('Entradas.store');

Route::get('Entradas/{entrada}/editar', 'ControladorEntradas@edit')->name('Entradas.edit');

Route::match(['put', 'patch'], 'Entradas/{entrada}/editar', 'ControladorEntradas@update')->name('Entradas.update');

Route::delete('Entradas/{entrada}', 'ControladorEntradas@destroy')->name('Entradas.destroy');
Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::get('Entradas/{entrada}/detalle', 'ControladorEntradas@detail')->name('Entradas.detail');

// Detalle Entradas

Route::get('detalleEntrada', 'ControladorDetalleEntrada@index')->name('detalleEntrada.index');


 // Datalle Proveedores

Route::get('proveedores', 'ControladorProveedores@index')->name('proveedores.index');

Route::get('proveedores/crear', 'ControladorProveedores@create')->name('proveedores.create');

Route::post('proveedores', 'ControladorProveedores@store')->name('proveedores.store');

Route::get('proveedores/{proveedor}/editar', 'ControladorProveedores@edit')->name('proveedores.edit');

Route::match(['put', 'patch'], 'proveedores/{proveedor}/editar', 'ControladorProveedores@update')->name('proveedores.update');

Route::delete('proveedor/{proveedor}', 'ControladorProveedores@destroy')->name('proveedores.destroy');
Auth::routes();


